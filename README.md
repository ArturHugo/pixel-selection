# Pixel Selection

Esse projeto foi desenvolvido usando OpenCV 3.4.4 and Numpy 1.11.1.
O código fonte do programa está dentro da pasta "src/" e os requisitos estão divididos da seguinte forma:
- Requisito 1: select_pixel.py
- Requisito 2: highlight_image.py
- Requisito 3: highlight_video.py
- Requisito 4: highlight_webcam.py

Para rodar cada script use as seguintes linhas no terminal:
- 1: python select_pixel.py
- 2: python highlight_image.py
- 3: python highlight_video.py
- 4: python highlight_webcam.py

A imagem e o vídeo utilizados no código estão na pasta "data/". Para testar os três primeiros scripts com outros arquivos, coloque o video avi ou a imagem jpg desejada no diretório "data/" com o nome video.mp4 ou imagem.jpg.