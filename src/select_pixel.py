import numpy as np
import cv2 as cv

from image_controller import ImageController


WINDOW_NAME = 'main window'

# Opening window, reading image and passing them to the controller
cv.namedWindow(WINDOW_NAME, cv.WINDOW_NORMAL)
IMAGE = cv.imread('data/imagem.jpg', cv.IMREAD_UNCHANGED)
controller = ImageController(IMAGE, WINDOW_NAME)


# Seting the callback of the window
def selection_callback(event, x, y, flags, param):
    if event == cv.EVENT_LBUTTONDOWN:
        param.pixel_selection(x, y)
        pixel = param.selected_pixel
        print('X: {} Y: {}'.format(pixel.x, pixel.y))
        if param.is_gray:
            print('Gray: {}'.format(pixel.gray))
        else:
            print('R: {} G: {} B: {}'.format(pixel.r, pixel.g, pixel.b))


cv.setMouseCallback(WINDOW_NAME, selection_callback, controller)

while(1):
    cv.imshow(WINDOW_NAME, controller.image)
    if cv.waitKey(20) & 0xFF == 27:
        break

cv.destroyAllWindows()
