import numpy as np
import cv2 as cv

from image_controller import ImageController


WINDOW_NAME = 'main window'

# Opening window and reading video
cv.namedWindow(WINDOW_NAME, cv.WINDOW_NORMAL)
VIDEO = cv.VideoCapture('data/video.mp4')

# Seting the callback of the window
def selection_callback(event, x, y, flags, param):
    if event == cv.EVENT_LBUTTONDOWN:
        param.pixel_selection(x, y)


ret, frame = VIDEO.read()
controller = ImageController(frame, WINDOW_NAME)
cv.setMouseCallback(WINDOW_NAME, selection_callback, controller)

#Frame counter starts on 1 beacause the frame 0 has already been read.
frame_counter = 1
while(1):
    ret, frame = VIDEO.read()
    frame_counter += 1
    if frame_counter == VIDEO.get(cv.CAP_PROP_FRAME_COUNT) -1:
        frame_counter = 0 
        VIDEO.set(cv.CAP_PROP_POS_FRAMES, 0)
    controller.image = frame
    if(controller.selected_pixel != None):
        highlighted_image = controller.highlight_pixels(controller.selected_pixel)
        cv.imshow(WINDOW_NAME, highlighted_image)
    else:
        cv.imshow(WINDOW_NAME, controller.image)
    if cv.waitKey(20) & 0xFF == 27:
        break

cv.destroyAllWindows()
VIDEO.release()