import numpy as np
import cv2 as cv

class ColoredPixel:
    def __init__(self, x, y, r, g, b):
        self.x = x
        self.y = y
        self.r = r
        self.g = g
        self.b = b

class GrayPixel:
    def __init__(self, x, y, gray):
        self.x = x
        self.y = y
        self.gray = gray

# Class to controll state of image displayed in opencv window
class ImageController:
    def __init__(self, image, window):
        self.image = image
        if(len(image.shape) == 3):
            self.is_gray = False
        else:
            self.is_gray = True
        self.window = window
        self.selected_pixel = None

    def pixel_selection(self, x, y):
        if self.is_gray:
            self.selected_pixel = GrayPixel(x, y, self.image.item(y, x))
        else:
            self.selected_pixel = ColoredPixel(x, y, self.image.item(y, x, 2),
                                                self.image.item(y, x, 1), 
                                                self.image.item(y, x, 0))
            


    def highlight_pixels(self, selected_pixel):
        if self.is_gray:
            dists = np.abs(self.image - selected_pixel.gray)
            image = np.repeat(self.image[..., None], 3, axis = 2)
        else:
            image = self.image
            pixel = [selected_pixel.b, selected_pixel.g, selected_pixel.r]
            dists = np.linalg.norm(image - pixel, axis = 2)

        red_matrix = np.zeros((image.shape[0], image.shape[1], 3)) + (0, 0, 255)
        red_mask = np.multiply(np.repeat(np.where(dists < 13, 1, 0)[..., None], 3, axis = 2), red_matrix)
        black_mask = np.repeat(np.where(dists < 13, 0, 1)[..., None], 3, axis = 2)
        highlighted_image = np.multiply(image, black_mask) + red_mask
        return highlighted_image.astype('uint8')

    def refresh_window(self, new_image):
        cv.imshow(self.window, new_image)
